//^ Index is landing page, this is the members page. This is for paid content.

import { useState } from "react";
import { useRouter } from "next/router";
import { Auth } from "aws-amplify";
import SignIn from "../components/Auth/SignIn";

interface MemberObject {
  username: null | string;
  challengeName: null | string;
}

export default function Members() {
  const [user, setUser] = useState<MemberObject>();
  const router = useRouter();

  async function LogOut() {
    try {
      await Auth.signOut();
      router.push("/");
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <main>
      {user ? (
        <>
          <h1>Members</h1>
          <button onClick={LogOut}>Sign Out</button>
        </>
      ) : (
        <SignIn setUser={setUser} />
      )}
    </main>
  );
}
