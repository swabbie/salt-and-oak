export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "saltandoakeb3f443d": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "function": {
        "saltandoak55e92eaa": {
            "Name": "string",
            "Arn": "string",
            "Region": "string",
            "LambdaExecutionRole": "string"
        }
    },
    "api": {
        "api173bfb7d": {
            "RootUrl": "string",
            "ApiName": "string",
            "ApiId": "string"
        }
    }
}