//@ setUser brought in from members page is set to any. Dispatch<SetStateAction<null>> were used previous

import { FormEvent, useState } from "react";
import { Auth } from "aws-amplify";
import SubscribeBtn from "../SubscribeBtn";

import AuthStyles from "../../styles/Auth.module.css";

export default function SignIn({ setUser }: any) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [checkAccount, setCheckAccount] = useState(false);

  async function login(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    try {
      const user = await Auth.signIn(username, password);
      if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
        setCheckAccount(true);
        const { requiredAttributes } = user.challengeParam;
        await Auth.completeNewPassword(user, newPassword);
      }
      setUser(user);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <form className={AuthStyles.auth__form} onSubmit={login}>
      <label className={AuthStyles.auth__label} htmlFor="username">
        Username
      </label>
      <input
        className={AuthStyles.auth__input}
        type="text"
        name="username"
        id="username"
        placeholder="username"
        onChange={(e) => setUsername(e.target.value)}
      />

      <label className={AuthStyles.auth__label} htmlFor="username">
        Password
      </label>
      <input
        className={AuthStyles.auth__input}
        type="password"
        name="Password"
        id="password"
        placeholder="password"
        onChange={(e) => setPassword(e.target.value)}
      />

      {checkAccount ? (
        <>
          <p>
            This is your first time logging in, please change your password.
          </p>
          <label className={AuthStyles.auth__label} htmlFor="username">
            New Password
          </label>
          <input
            className={AuthStyles.auth__input}
            type="password"
            name="newPassword"
            id="newPassword"
            placeholder="new password"
            onChange={(e) => setNewPassword(e.target.value)}
          />
        </>
      ) : (
        <></>
      )}
      <div className={AuthStyles.auth__btn__wrap}>
        <div className={AuthStyles.auth__p__wrap}>
        <p>Welcome! Please sign in.</p>
        <p>If you are not a member, please subscribe.</p>
        </div>
        <button className="signin__btn">SignIn</button>
        <SubscribeBtn />
      </div>
    </form>
  );
}
