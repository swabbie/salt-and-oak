//^ hero section for landing page
import Link from "next/link";
import Image from "next/image";
import LandingStyle from "../../styles/Landing.module.css";

export default function LandingHero() {
  return (
    <section className={LandingStyle.st__container__white}>
      <div className={LandingStyle.st__wrap}>
        <div className={LandingStyle.st__hero}>
          <div className={LandingStyle.st__hero__title__wrap}>
            <h1 className={LandingStyle.st__hero__title}>Salt and Oak</h1>
            <h3 className={LandingStyle.st__hero__subtitle}>
              Journey of a wooden boat
            </h3>
            <p>
              Fan site for{" "}
              <a
                href="https://www.youtube.com/c/SaltTar"
                target="_blank"
                rel="noreferrer">
                Salt and Tar.
              </a>
            </p>
            <p>
              built and maintained by{" "}
              <a
                href="https://www.dev-mason-roberts.com"
                target="_blank"
                rel="noreferrer">
                Dev3027
              </a>
            </p>
            <div className={LandingStyle.st__hero__action__wrap}>
              <a href="#promo" className="primary__btn">
                Learn More
              </a>
              <a href="#video" className="secondary__btn">
                Preview Access
              </a>
            </div>
            <Link href="members">
              <a>
                <button className="signin__btn">Sign In</button>
              </a>
            </Link>
          </div>
          <div className={LandingStyle.st__hero__img__wrap}>
            <Image
              className={LandingStyle.st__hero__img}
              src="/images/hero.png"
              alt="Salt and Tar"
              width={240}
              height={349}
            />
          </div>
        </div>
        <div className={LandingStyle.st__hero__banner__wrap}>
          <Image
            src="/images/Group 28.svg"
            alt="decoration"
            width={1024}
            height={170}
          />
        </div>
      </div>
    </section>
  );
}
