//^ Video section of landing page
import Image from 'next/image';
import SubscribeBtn from "../SubscribeBtn";
import LandingStyle from "../../styles/Landing.module.css";
import Link from "next/link";

export default function LandingVideo() {
  return (
    <section className={LandingStyle.st__container__video}>
      <div className={LandingStyle.st__video__begin}></div>
      <div className={LandingStyle.st__container__video}>
        <div id="video" className={LandingStyle.st__wrap__video}>
          <div className="flex flex-col md:w-1/2">
            <div className="flex flex-col w-full justify-center items-center">
              <div className="flex flex-col items-center w-[420px]">
                <div className="flex w-full sm:justify-end sm:-mb-5">
                  <span className="font-fell text-4xl text-white  ml-6 sm:ml-0 sm:text-end">
                    Salt and Tar
                  </span>
                </div>
                <div className="flex justify-start w-full">
                  <h1 className="font-fell text-9xl text-white ml-6 sm:ml-0">
                    Video
                  </h1>
                </div>
              </div>
            </div>
            <div className="flex flex-col items-center sm:block">
              <div className="flex flex-col items-center md:hidden bg-white rounded-xl w-[350px] h-full">
                <p className="px-3 py-7">
                  The videos here are the same ones found on the YouTube channel
                  and use the same url. They are just presented in a different
                  style here. Here you will find only Salt and Tar content
                  organized into easy to follow playlists. Subscribe to get
                  access to full content and 10% of your subscription will be
                  donated to Salt and Tar. Follow and enjoy the journey in a
                  whole new way. Become a part of the crew and support them as
                  you follow the journey of building and sailing a wooden boat.
                </p>
                <div className="flex w-full justify-evenly">
                  <Link href="preview">
                    <a>
                      <button className="action__btn">Preview</button>
                    </a>
                  </Link>
                  <SubscribeBtn />
                </div>
              </div>
              <div className="md:relative my-8 z-10 lg:ml-32">
                <img
                  src="/images/Video_Home.png"
                  alt="border bottom"
                  className="border-4 border-st-secondary rounded-lg md:absolute top-0 left-8"
                  width={350}
                />
              </div>
            </div>
          </div>

          <div className="hidden md:flex justify-center md:w-1/2">
            <div className="hidden md:flex flex-col items-center bg-white rounded-xl w-[350px] h-full">
              <p className="px-3 py-7">
                The videos here are the same ones found on the YouTube channel
                and use the same url. They are just presented in a different
                style here. Here you will find only Salt and Tar content
                organized into easy to follow playlists. Subscribe to get access
                to full content and 10% of your subscription will be donated to
                Salt and Tar. Follow and enjoy the journey in a whole new way.
                Become a part of the crew and support them as you follow the
                journey of building and sailing a wooden boat.
              </p>
              <div className="flex w-full justify-evenly">
                <Link href="preview">
                  <a>
                    <button className="action__btn">Preview</button>
                  </a>
                </Link>
                <SubscribeBtn />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
