import type { AppProps } from 'next/app';
import config from '../src/aws-exports';
import { Amplify } from "aws-amplify";
import '../styles/globals.css';

Amplify.configure(config);

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
