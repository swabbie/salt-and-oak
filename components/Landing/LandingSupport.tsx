//^ Support section for landing page
import Image from "next/image";
import LandingStyle from "../../styles/Landing.module.css";

export default function LandingSupport() {
  return (
    <section className={LandingStyle.st__container__ltPrimary}>
      <div className={LandingStyle.st__support__wrap}>
        <div className={LandingStyle.st__support__title__wrap}>
          <div>
            <h1 className={LandingStyle.st__support__title}>New</h1>
          </div>
          <div>
            <Image
              className={LandingStyle.st__support__logo}
              src="/images/logo.svg"
              alt="star logo"
              width={100}
              height={100}
            />
          </div>
          <div>
            <h1 className={LandingStyle.st__support__title}>Wind</h1>
          </div>
        </div>
        <div className="max-w-md px-2 mb-3">
          <p className={LandingStyle.st__support_p}>
            This is a fan site built by me, Developer3027. Below you can find
            different ways to help Salt and Tar build Rediviva.
          </p>
        </div>
        <div className="flex mb-4 w-full">
          <div className="md:flex flex-col justify-evenly w-full md:w-1/2 hidden">
            <div className="md:flex justify-evenly hidden my-3 w-full">
              <a
                href="https://www.youtube.com/c/SaltTar"
                target="_blank"
                rel="noreferrer">
                <div className={LandingStyle.st__support__infocard}>
                  <h1 className={LandingStyle.st__support__infotitle}>
                    YouTube
                  </h1>
                  <p className={LandingStyle.st__support__infosub}>69K</p>
                  <small className={LandingStyle.st__support__infosmall}>
                    subscribers
                  </small>
                </div>
              </a>

              <a
                href="https://www.instagram.com/saltandtar/"
                target="_blank"
                rel="noreferrer">
                <div className={LandingStyle.st__support__infocard}>
                  <h1 className={LandingStyle.st__support__infotitle}>
                    Instagram
                  </h1>
                  <p className={LandingStyle.st__support__infosub}>6K</p>
                  <small className={LandingStyle.st__support__infosmall}>
                    followers
                  </small>
                </div>
              </a>
            </div>

            <div className="md:flex justify-evenly hidden my-3 w-full">
              <a
                href="https://www.patreon.com/saltandtar"
                target="_blank"
                rel="noreferrer">
                <div className={LandingStyle.st__support__infocard}>
                  <h1 className={LandingStyle.st__support__infotitle}>
                    Patreon
                  </h1>
                  <p className={LandingStyle.st__support__infosub}>283</p>
                  <small className={LandingStyle.st__support__infosmall}>
                    members
                  </small>
                </div>
              </a>
              <div className={LandingStyle.st__support__infocard}>
                <h1 className={LandingStyle.st__support__infotitle}>
                  Supporters
                </h1>
                <p className={LandingStyle.st__support__infosub}>100%</p>
                <small className={LandingStyle.st__support__infosmall}>
                  satisfied
                </small>
              </div>
            </div>
          </div>
          <div className="flex flex-col justify-around w-full md:w-1/2">
            <a
              href="https://www.patreon.com/saltandtar"
              target="_blank"
              rel="noreferrer">
              <div className="flex flex-col w-full pl-2 border-l-4 border-st-secondary mx-1 my-2">
                <h1 className={LandingStyle.st__support__infotitle}>Patreon</h1>
                <p className={LandingStyle.st__support_p}>
                  A membership platform used to provide rewards and perks to
                  subscribers.
                </p>
              </div>
            </a>
            <a
              href="https://www.youtube.com/c/SaltTar"
              target="_blank"
              rel="noreferrer">
              <div className="flex flex-col w-full pl-2 border-l-4 border-st-secondary mx-1 my-2">
                <h1 className={LandingStyle.st__support__infotitle}>YouTube</h1>
                <p className={LandingStyle.st__support_p}>
                  A video sharing platform used to share the journey of building
                  Rediviva.
                </p>
              </div>
            </a>
            <a
              href="https://www.instagram.com/saltandtar/"
              target="_blank"
              rel="noreferrer">
              <div className="flex flex-col w-full pl-2 border-l-4 border-st-secondary mx-1 my-2">
                <h1 className={LandingStyle.st__support__infotitle}>
                  Instagram
                </h1>
                <p className={LandingStyle.st__support_p}>
                  A image sharing platform used to share pictures of the
                  adventures in building Rediviva.
                </p>
              </div>
            </a>
            <a
              href="https://www.gofundme.com/f/saltandtar"
              target="_blank"
              rel="noreferrer">
              <div className="flex flex-col w-full pl-2 border-l-4 border-st-secondary mx-1 my-2">
                <h1 className={LandingStyle.st__support__infotitle}>
                  GoFundMe
                </h1>
                <p className={LandingStyle.st__support_p}>
                  A crowdfunding platform used to raise funding for building
                  Rediviva.
                </p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
