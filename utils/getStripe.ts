//^ This utils function is used to not load stripe until it is asked for by the button
import { Stripe, loadStripe } from "@stripe/stripe-js";

let stripePromise: Promise<Stripe | null>;
const getStripe = () => {
  if (!stripePromise) {
    stripePromise = loadStripe(`${process.env.stripe_test_public_key}`);
  }
  return stripePromise;
};

export default getStripe;
