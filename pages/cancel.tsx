import React from 'react'

export default function cancel() {
  return (
    <div>You canceled the order either by navigating away before the transaction was complete or there was some error. You were not charged. Sorry for the inconvenience.</div>
  )
}
