# Salt and Oak => Salt and Tar fan site

Find Salt and Tar at [Salt and Tar](https://www.youtube.com/c/SaltTar) on YouTube.

This full stack app is built with NextJS. Uses various AWS services. Styled with TailwindCss.

Notion doc used to organize project and build [Salt and Oak](https://developer3027.notion.site/Salt-and-Oak-3605cf4b02a9440eb2b6ca5360120d2f)

#### Note
You may find funny comment syntax because I use BetterComments and uses code spell checker.
