/* cSpell:ignore producta */
/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    stripe_test_public_key: process.env.STRIPE_TEST_PUBLIC_KEY,
    stripe_test_producta_price: process.env.STRIPE_TEST_PRODUCTA_PRICE,
  },
};

module.exports = nextConfig
