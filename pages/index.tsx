import type { NextPage } from 'next'
import Head from 'next/head'
import LandingHero from "../components/Landing/LandingHero"
import LandingPromo from "../components/Landing/LandingPromo"
import LandingSupport from "../components/Landing/LandingSupport"
import LandingVideo from "../components/Landing/LandingVideo"

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Salt and Oak</title>
        <meta charSet="utf-8" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <meta
          name="viewport"
          content="width=device-width, height=device-height, viewport-fit=cover, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no"
        />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="author" content="Mason Roberts" />
        <meta
          name="description"
          content="Salt and Oak is a fan site built by Mason Roberts. Enjoy the journey of Rediviva and her crew from it's humble beginnings in Washington state to straight course on the open water.  This concept is provided as a concept for content providers looking to have a more all in one solution."
        />
        <meta property="og:url" content="https://mason-roberts.com" />
        <meta property="og:title" content="Developer3027" />
        <meta
          property="og:description"
          content="Salt and Oak is a fan site built by Mason Roberts. Enjoy the journey of Rediviva and her crew from it's humble beginnings in Washington state to straight course on the open water.  This concept is provided as a concept for content providers looking to have a more all in one solution."
        />
        <meta
          property="og:image"
          content="https://mason-roberts.com/images/card-image.png"
        />
        <meta
          name="twitter:card"
          content="Salt and Oak is a fan site built by Mason Roberts. Enjoy the journey of Rediviva and her crew from it's humble beginnings in Washington state to straight course on the open water.  This concept is provided as a concept for content providers looking to have a more all in one solution."
        />
      </Head>

      <main>
        <LandingHero />
        <LandingSupport />
        <LandingPromo />
        <LandingVideo />
      </main>

    </div>
  );
}

export default Home
