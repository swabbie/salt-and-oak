//^ Promo section to explain membership
import { useState } from "react";
import SubscribeBtn from "../SubscribeBtn";
import LandingStyle from "../../styles/Landing.module.css";

export default function LandingPromo() {
  const [flip, setFlip] = useState(true);
  return (
    <section id="promo" className={LandingStyle.st__container__ltPrimary}>
      <div className={LandingStyle.st__support__wrap}>
        <div className={LandingStyle.st__promo__card}>
          {flip ? (
            <div
              className="flex__col__center__full"
              onClick={() => setFlip(!flip)}>
              <div className={LandingStyle.st__promo__header}>
                <h1 className={LandingStyle.st__promo__title}>Join Today</h1>
              </div>
              <div className="flex__center">
                <h1 className={LandingStyle.st__promo__title}>$ 19.99</h1>
                <small>&nbsp;usd</small>
              </div>
              <div>
                <h3 className={LandingStyle.donateListHead}>Membership</h3>
                <ul className={LandingStyle.donateList}>
                  <li>Support the crew</li>
                  <li>Just Salt and Tar</li>
                  <li>Privately Managed</li>
                  <li>Organized playlists</li>
                  <li>No YouTube distractions</li>
                </ul>
              </div>
            </div>
          ) : (
            <div
              className="flex__col__center__full"
              onClick={() => setFlip(!flip)}>
              <div className={LandingStyle.st__promo__header}>
                <h1 className={LandingStyle.donateListHead}>Membership</h1>
              </div>
              <div>
                <ul className={LandingStyle.donateList}>
                  <li>
                    Part of your membership is donated to the crew monthly.
                  </li>
                  <li>Video links to YouTube, watch here or there.</li>
                  <li>Designed to clone popular streaming services.</li>
                  <li>No distraction from YouTube, enjoy only Salt and Tar.</li>
                  <li>Managed by Dev3027, updated weekly</li>
                </ul>
              </div>
            </div>
          )}
          <SubscribeBtn />
        </div>
      </div>
    </section>
  );
}
