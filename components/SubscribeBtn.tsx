// cSpell:ignore producta
import getStripe from "../utils/getStripe";

export default function SubscribeBtn() {
  const handleSubscription = async () => {
    const stripe = await getStripe();
    const { error } = await stripe!.redirectToCheckout({
      lineItems: [
        {
          price: `${process.env.stripe_test_producta_price}`,
          quantity: 1,
        },
      ],
      mode: "subscription",
      successUrl: "http://localhost:3000/members",
      cancelUrl: "http://localhost:3000/cancel",
    });
    console.warn(error.message);
  }

  return (
    <button onClick={handleSubscription} className="subscribe__btn">
      Get Access
    </button>
  );
}
