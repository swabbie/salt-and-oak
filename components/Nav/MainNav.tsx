//^ cut from the main page, this needs to be reworked into a nav bar.

export default function MainNav() {
  return (
    <section id="home" className={styles.st__header__section}>
      <header className={styles.st__header}>
        <div className={styles.st__navbar}>
          <div className={styles.st__nav__container}>
            <a href="#home" className={styles.st__brand}>
              <img className={styles.st__nav__logo} src="/images/st/logo.svg" />
            </a>
          </div>
        </div>
      </header>
    </section>
  );
}
